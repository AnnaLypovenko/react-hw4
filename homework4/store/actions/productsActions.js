export const openModalFavorite = (id) => ({type: "openModalFavorite", id: id});
export const closeModalFavorite = () => ({type: "closeModalFavorite"});

export const openModalCart = (id) => ({type: "openModalCart", id: id});
export const closeModalCart = () => ({type: "closeModalCart"});

export const changeFavoriteArray  = () => ({type: "changeFavoriteArray"});
export const changeCartArray = (newArray) => ({type: "changeCartArray", payload: newArray});

export const changeProductsArray = (newArray) => ({type: "changeProductsArray", payload: newArray});
