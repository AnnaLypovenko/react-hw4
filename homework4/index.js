import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from "./store/configureStore"
import {createStore} from "redux";
import reducer from './store/reducers/rootReducer';
import {Provider} from 'react-redux'


ReactDOM.render(
    <Provider store={store}>
       <App/>
    </Provider>
    ,
    document.getElementById('root')
);
