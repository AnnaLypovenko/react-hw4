import React from 'react';

export function Cart({filled}) {

  if (!filled) {
    return (
        <img className="cart" src="./images/recycle_empty.png" alt=""/>
    );
  }

  return (
      <img className="cart" src="./images/recycle_full.png" alt=""/>
  );
}
